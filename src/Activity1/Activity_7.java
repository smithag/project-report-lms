package Activity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_7 {
	 WebDriver driver;
	@BeforeClass
    public void beforeClass() {
   	driver = new FirefoxDriver();
		
	//Open the browser
	driver.get("https://alchemy.hguy.co/lms");
    }
	
    @Test
    public void TestCase() {
    	//Find the navigation bar.
        String navbar = driver.findElement(By.cssSelector("#site-navigation")).getText();
        
   		System.out.println("navigation bar is:" + navbar);
   		
   		//Select the menu item that says �All Courses� and click it.
   		driver.findElement(By.linkText("All Courses")).click();
   		
   		//Find all the courses on the page.
   		
   		List<WebElement> courses = driver.findElements(By.className("entry-title"));	 
   		
   		//Print the number of courses on the page.
   		System.out.println("number of courses on the page is:"+courses.size() );
    }
    
    @AfterClass
    public void afterClass() {
    driver.close();
    }
    }
    
