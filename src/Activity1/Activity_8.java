package Activity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_8 {
	 WebDriver driver;
	@BeforeClass
    public void beforeClass() {
   	driver = new FirefoxDriver();
		
	//Open the browser
	driver.get("https://alchemy.hguy.co/lms");
    }
	
    @Test
    public void TestCase() {
    
    	//Find the navigation bar.
        String navbar = driver.findElement(By.cssSelector("#site-navigation")).getText();
        
   		System.out.println("navigation bar is:" + navbar);
   		
   		//Select the menu item that says �Contact� and click it.
   		driver.findElement(By.linkText("Contact")).click();
   		
   		//Find the contact form fields (Full Name, email, etc.)
   		
   		WebElement name= driver.findElement(By.xpath("//input[@id='wpforms-8-field_0']"));
   		WebElement email= driver.findElement(By.xpath("//input[@id='wpforms-8-field_1']"));
   		//WebElement subject= driver.findElement(By.xpath("//input[@id='wpforms-8-field_3']"));
   		WebElement comment= driver.findElement(By.name("wpforms[fields][2]"));
   		
   		//Fill in the information and leave a message.
   		name.sendKeys("Smitha");
   		email.sendKeys("smitha@gmail.com");
   		//subject.sendKeys("Hello");
   		comment.sendKeys("Details added");
 
   		//Click submit
   		driver.findElement(By.xpath("//button[contains(@type, 'submit')]")).click();
    
   		//Read and print the message displayed after submission.
    
    String Message = driver.findElement(By.id("wpforms-confirmation-8")).getText();
    System.out.println("Contact Form submitted is: " + Message);

    }
   @AfterClass
    public void afterClass() {
    driver.close();
    }
}
