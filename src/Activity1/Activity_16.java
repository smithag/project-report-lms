package Activity1;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity_16 {
 
	 WebDriver driver;
		@BeforeClass
	    public void beforeClass() {
	   	driver = new FirefoxDriver();
	   				
		//Open the browser
		driver.get("https://alchemy.hguy.co/lms");
		}
		 @Test(priority=0)
		    public void TestCase1() {
		    //Find the navigation bar.
			  String navbar = driver.findElement(By.xpath("//*[@id='site-navigation']")).getText();
		        
		   		System.out.println("navigation bar is:" + navbar);
		   		driver.findElement(By.xpath("//*[@id='menu-item-1507']")).click();
		   	    driver.findElement(By.xpath("//*[contains(text(),'Login')]")).click();
		   	    driver.findElement(By.xpath("//*[@id='user_login']")).sendKeys("root"); 
		   	    driver.findElement(By.xpath("//*[@id='user_pass']")).sendKeys("pa$$w0rd"); 
		        driver.findElement(By.xpath("//*[@id='wp-submit']")).click();
	//Select the menu item that says �All Courses� and click it.
		   		driver.findElement(By.xpath("//*[@id='menu-item-1508']")).click();
		 }
		 
		 @Test(priority=1)
		    public void TestCase2() {
			 
			 String cTitle1=driver.findElement(By.cssSelector("#post-24042 > div:nth-child(3) > h3:nth-child(1)")).getText();
			 System.out.println("Course Title is : "+ cTitle1);
			 driver.findElement(By.cssSelector("#post-24042 > div:nth-child(3) > p:nth-child(3) > a:nth-child(1)")).click();
		     
			 driver.findElement(By.cssSelector("#ld-expand-283 > div:nth-child(1) > a:nth-child(1) > div:nth-child(2)")).click();
		   String cLesson1=driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
		   System.out.println("Title of the Lesson1 is : "+ cLesson1);
		   
		   driver.findElement(By.cssSelector("div.ld-content-action:nth-child(3) > a:nth-child(1) > span:nth-child(1)")).click();
		   
		 }
		 @Test(priority=2)
		    public void TestCase3() {
			 String cTitle2=driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
			 System.out.println("Course2 Title is : "+ cTitle2);
			 
			driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div/div[3]/div/div[2]/div[1]/a/span")).click();
			String cLesson2=driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/h1")).getText();
			System.out.println("Title of the Lesson2 is : "+ cLesson2);
			driver.findElement(By.cssSelector("div.ld-content-action:nth-child(3) > a:nth-child(1)")).click();
			
			String cLesson3=driver.findElement(By.cssSelector(".ld-focus-content > h1:nth-child(1)")).getText();
			System.out.println("Title of the Lesson3 is : "+ cLesson3);
			 
			 driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div/div[3]/a")).click();
						 
			 driver.findElement(By.xpath("/html/body/div[2]/div/div[2]/div[2]/div/div/div[4]/a")).click();
		 }
		 @Test(priority=3)
		    public void TestCase4() {
			 
			String cStatus= driver.findElement(By.cssSelector(".ld-progress-percentage")).getText();
			System.out.println("Course completion status is : "+ cStatus);		
			 
		 }
		 
		 @AfterClass
		    public void afterClass() {
		 driver.close();
		    }
		    }
