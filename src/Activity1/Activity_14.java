package Activity1;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;


public class Activity_14 {
	 WebDriver driver;
		@BeforeClass
	    public void beforeClass() {
	   	driver = new FirefoxDriver();
	   	
			
		//Open the browser
		driver.get("https://alchemy.hguy.co/lms");
		}
		  
		  //Open a course link.
		  @Test(priority=1)
		    public void TestCase1() {
			  Actions actions = new Actions(driver);
			  JavascriptExecutor js = (JavascriptExecutor) driver;
			  js.executeScript("window.scrollBy(221,714)");
			  WebDriverWait wait = new WebDriverWait(driver,5);
			  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@id='post-69']")));
			  WebElement course=driver.findElement(By.xpath("//*[@id='post-69']"));
			  actions.sendKeys(course, Keys.ENTER).build().perform();
		  }
		  
		  //Enroll for course
		  @Test(priority=2)
		    public void TestCase2() throws InterruptedException {
			  Actions actions = new Actions(driver);
			  WebDriverWait wait = new WebDriverWait(driver,15);
			  wait.until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".ld-course-status")));
			  WebElement button = driver.findElement(By.cssSelector(".ld-course-status"));
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", button);
			  WebElement login1= driver.findElement(By.cssSelector(".ld-button"));
				actions.sendKeys(login1, Keys.ENTER).build().perform();	   	
			WebElement username =   driver.findElement(By.id("user_login"));
		   	 WebElement passwd =   driver.findElement(By.id("user_pass")); 
		   	 actions.sendKeys(username,"root").perform();
		     actions.sendKeys(passwd,"pa$$w0rd").perform();
		    WebElement loginForm=driver.findElement(By.xpath("//*[@id='loginform']/p[4]"));
		    actions.sendKeys(loginForm, Keys.ENTER).build().perform();
		 
		}
		  //Learn Topics
		  @Test(priority=3)
		    public void TestCase3() throws InterruptedException {
			  Actions actions = new Actions(driver);
			  Thread.sleep(2000);
//		    WebDriverWait wait = new WebDriverWait(driver,5);
//			  wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector("div.ld-item-list-item:nth-child(1) > div:nth-child(1) > a:nth-child(1)")));
		      WebElement topic1 = driver.findElement(By.cssSelector("div.ld-item-list-item:nth-child(1) > div:nth-child(1) > a:nth-child(1)"));
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", topic1);
			  actions.sendKeys(topic1, Keys.ENTER).build().perform();	
		  }
		  
		  //Lessons under topic1
		  @Test(priority=4)
		    public void TestCase4() throws InterruptedException {
			  Actions actions = new Actions(driver);
			  Thread.sleep(1000);
			  WebElement lesson1=driver.findElement(By.cssSelector("#ld-table-list-item-175 > a:nth-child(1) > span:nth-child(2)"));
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", lesson1);
			  actions.sendKeys(lesson1, Keys.ENTER).build().perform();	
			  WebElement lesson2=driver.findElement(By.cssSelector("#ld-nav-content-list-83 > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > a:nth-child(1) > div:nth-child(2)"));
			  actions.sendKeys(lesson2, Keys.ENTER).build().perform();	
			  WebElement lesson3=driver.findElement(By.cssSelector("div.ld-table-list-item:nth-child(3) > a:nth-child(1) > div:nth-child(2)"));
			  actions.sendKeys(lesson3, Keys.ENTER).build().perform();
			  WebElement lessonList=driver.findElement(By.cssSelector("a.ld-primary-color"));
			  actions.sendKeys(lessonList, Keys.ENTER).build().perform();
		  }
		  
		//Topic2
		  @Test(priority=5)
		    public void TestCase5() throws InterruptedException {
			  Actions actions = new Actions(driver);
			  Thread.sleep(2000);
			  WebElement topic2 = driver.findElement(By.cssSelector("div.ld-item-list-item:nth-child(2) > div:nth-child(1) > a:nth-child(1)"));
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", topic2);
			  actions.sendKeys(topic2, Keys.ENTER).build().perform();	
		  }
		  
		//Lessons under topic2
		  @Test(priority=6)
		    public void TestCase6() throws InterruptedException {
			  Actions actions = new Actions(driver);
			  Thread.sleep(1000);
			  WebElement lesson4=driver.findElement(By.cssSelector("#ld-table-list-item-200 > a:nth-child(1) > span:nth-child(2)"));
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", lesson4);
			  actions.sendKeys(lesson4, Keys.ENTER).build().perform();	
			  WebElement lesson5=driver.findElement(By.cssSelector("#ld-nav-content-list-85 > div:nth-child(1) > div:nth-child(1) > div:nth-child(2) > a:nth-child(1) > div:nth-child(2)"));
			  actions.sendKeys(lesson5, Keys.ENTER).build().perform();	
			  
			  WebElement lessonList=driver.findElement(By.cssSelector("a.ld-primary-color"));
			  actions.sendKeys(lessonList, Keys.ENTER).build().perform();
		  }
		  
		//Topic3
		  @Test(priority=7)
		    public void TestCase7() throws InterruptedException {
			  Actions actions = new Actions(driver);
			  Thread.sleep(2000);
			  WebElement topic2 = driver.findElement(By.cssSelector("#ld-expand-87 > div:nth-child(1) > a:nth-child(1) > div:nth-child(2)"));
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", topic2);
			  actions.sendKeys(topic2, Keys.ENTER).build().perform();	
		  }
		  
		  
		  //Back to course list
		  @Test(priority=8)
		    public void TestCase8() throws InterruptedException {
			  Actions actions = new Actions(driver);
			  Thread.sleep(2000);
			  WebElement bcourse=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/a"));
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", bcourse);
			  actions.sendKeys(bcourse, Keys.ENTER).build().perform();
		  }
		  
		  
		  //Check progress of course and print it
		  @Test(priority=9)
		    public void TestCase9() throws InterruptedException {
		
			  Thread.sleep(1000);
			  WebElement progress=driver.findElement(By.cssSelector(".ld-progress-percentage"));
			  ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", progress);
			  String cprogress = driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/div/div/div/div[1]")).getText();
				 System.out.println("Course completion status is : "+ cprogress);
		  }
		  

		@AfterClass
		    public void afterClass() {
		  driver.close();
		    }
		    }



      
		   		
