package Activity1;

import org.testng.annotations.Test;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Acitivity_2 {
	 WebDriver driver;
	@BeforeClass
    public void beforeClass() {
   	driver = new FirefoxDriver();
		
	//Open the browser
	driver.get("https://alchemy.hguy.co/lms");
    }
	
    @Test
    public void TestCase1() {

   	 //Find the page title and print it
       String pageTitle = driver.getTitle();
       System.out.println("Page title is :" + pageTitle);
    }
    @Test
    public void TestCase2() {
       
       //Find the heading of the webpage and print
       String Header = driver.findElement(By.cssSelector("h1.uagb-ifb-title")).getText();
    //   Assert.assertEquals("Learn from Industry Experts",Header);
       Assert.assertTrue(true,"Learn from Industry Experts");
       System.out.println("Heading of the webpage is: " + Header);
    }
      
       @AfterClass
       public void afterClass() {
    	    driver.close();
    }
    
    }

