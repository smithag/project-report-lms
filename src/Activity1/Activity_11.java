package Activity1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;



public class Activity_11 {
 
	 WebDriver driver;
		@BeforeClass
	    public void beforeClass() {
	   	driver = new FirefoxDriver();
	   	
			
		//Open the browser
		driver.get("https://alchemy.hguy.co/lms");
		}
		  @Test(priority=0)
		    public void TestCase() {
		    //Find the navigation bar.
		    	    String navbar = driver.findElement(By.cssSelector("#site-navigation")).getText();
			        System.out.println("navigation bar is:" + navbar);
			   		driver.findElement(By.linkText("My Account")).click();
			   	    driver.findElement(By.linkText("Login")).click();
			   	    driver.findElement(By.id("user_login")).sendKeys("root");
			   	    driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd"); 
			        driver.findElement(By.className("login-submit")).click();
			   	//Select the menu item that says �All Courses� and click it.
			   		driver.findElement(By.linkText("All Courses")).click();
			   		
			    //Select any course and open it.
		             driver.findElement(By.xpath("//a[contains(@href,'social-media') and @role='button']")).click();
		             driver.findElement(By.className("ld-item-title")).click();
		             String Title = driver.getTitle();
			   	     System.out.println("Title of the Topic is : "+ Title);

		   		//Click on a lesson in the course. Verify the title of the course.
			   		 driver.findElement(By.linkText("This is the First Topic")).click();
			    	 String pageTitle1 = driver.getTitle();
			   	     System.out.println("Title of the lesson1 is : "+ pageTitle1);
			   		
			   	// Open a topic in that lesson. Mark it as complete.
			   	  driver.findElement(By.linkText("Next Topic")).click();
			      String pageTitle2 = driver.getTitle();
		  	      System.out.println("Title of the lesson2 is : "+ pageTitle2);
		  	      driver.findElement(By.linkText("Next Topic")).click();
		  	     driver.findElement(By.linkText("Back to Lesson")).click();
		  	     driver.findElement(By.partialLinkText("Basic Investment")).click();
		  	       String pageTitle3 = driver.getTitle();
			     System.out.println("Title of the lesson3 is : "+ pageTitle3);
			   	     
			   	//Mark all the topics in the lesson as complete (if available).
			      
			 	   String topicStatus1 = driver.findElement(By.cssSelector(".ld-status")).getText();
			     System.out.println("Topic1 completion status is : "+ topicStatus1);

			     //Perform the above steps for all lessons and topics in the course
		
			     driver.findElement(By.linkText("Back to Lesson")).click();
			    	
			    	driver.findElement(By.cssSelector("div.ld-lesson-item:nth-child(2)")).click();
			    	
			   	     driver.findElement(By.linkText("Next Lesson")).click();
			   	  String Title2 = driver.getTitle();
			   	     System.out.println("Title of the Topic2 is : "+ Title2);
			   	  driver.findElement(By.linkText("Success with Advert")).click();
			   		 String pageTitle4 = driver.getTitle();
			   		 
			   	     System.out.println("Title of the lesson4 is : "+ pageTitle4);
			   	  driver.findElement(By.cssSelector("div.ld-content-action:nth-child(3)")).click();
			   	     driver.findElement(By.linkText("Back to Lesson")).click();
			   	     driver.findElement(By.linkText("Relationships")).click();
			   	      String pageTitle5 = driver.getTitle();
			   	     System.out.println("Title of the lesson5 is : "+ pageTitle5);
			   	  driver.findElement(By.linkText("Back to Lesson")).click();
			   	  driver.findElement(By.cssSelector("div.ld-content-action:nth-child(3)")).click();
			   	    
			   	   	   String topicStatus2 = driver.findElement(By.cssSelector(".ld-status")).getText();
			   	
				     System.out.println("Topic2 completion status is : "+ topicStatus2);  
		        
			    String Title3 = driver.getTitle();
			    System.out.println("Title of the Topic3 is : "+ Title3);
			    
			    driver.findElement(By.linkText("Back to Course")).click();
			   driver.findElement(By.className("ld-progress-bar")).click();
			   String progress = driver.findElement(By.cssSelector(".ld-progress-percentage")).getText();
			   System.out.println("Course completion status is : "+ progress);
			   driver.findElement(By.xpath("//*[@id='menu-item-1507']")).click();
				String logout= driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[4]/a")).getText();
				System.out.println("logged out from Myaccount : "+ logout);
		      }
		  
			     @AfterClass
				    public void afterClass() {
				  driver.close();
				    }
				    }
