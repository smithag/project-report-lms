package Activity1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity_13 {
	WebDriver driver;
	@BeforeClass
    public void beforeClass() {
   	driver = new FirefoxDriver();
   	
		
	//Open the browser
	driver.get("https://alchemy.hguy.co/lms");
    }
	
    @Test
    public void TestCase() {
    //Find the navigation bar.
    	 //Find the navigation bar.
        String navbar = driver.findElement(By.xpath("//*[@id='site-navigation']")).getText();
        
   		System.out.println("navigation bar is:" + navbar);
   		driver.findElement(By.xpath("//*[@id='menu-item-1507']")).click();
   	    driver.findElement(By.xpath("//*[contains(text(),'Login')]")).click();
   	    driver.findElement(By.xpath("//*[@id='user_login']")).sendKeys("root"); 
   	    driver.findElement(By.xpath("//*[@id='user_pass']")).sendKeys("pa$$w0rd"); 
        driver.findElement(By.xpath("//*[@id='wp-submit']")).click();
   	//Select the menu item that says �All Courses� and click it.
   		driver.findElement(By.xpath("//*[@id='menu-item-1508']")).click();
   		
    //Select any course and open it.
        driver.findElement(By.xpath("//*[@id='post-69']")).click();
        String cTitle = driver.getTitle();
   	     System.out.println("Title of the course is : "+ cTitle);
        driver.findElement(By.xpath("//*[@class='ld-item-title']")).click();
        String Title = driver.getTitle();
   	     System.out.println("Title of the Topic is : "+ Title);
		//Click on a lesson in the course. Verify the title of the course.
   		 driver.findElement(By.xpath("//*[@class='ld-topic-title']")).click();
   
   		 String pageTitle = driver.getTitle();
   	     System.out.println("Title of the lesson1 is : "+ pageTitle);
   		
   	        		
   	  // Open a topic in that lesson. Mark it as complete.
   	       driver.findElement(By.xpath("//*[@class='ld-content-action']")).click();
   	       String pageTitle2 = driver.getTitle();
	       System.out.println("Title of the lesson2 is : "+ pageTitle2);
	       
	   
	       	driver.findElement(By.xpath("/html/body/div[1]/div/div[1]/div[2]/div/div/div/div/div[1]/div[2]/div/div/div[3]/a")).click();
	        String pageTitle3 = driver.getTitle();
            System.out.println("Title of the lesson3 is : "+ pageTitle3);
   	     
   	//Mark all the topics in the lesson as complete (if available).
      driver.findElement(By.xpath("//*[@class='ld-primary-color']")).click();
 	   String lessonStatus = driver.findElement(By.xpath("//*[@class='ld-lesson-list-progress']")).getText();
     System.out.println("Topic1 completion status is : "+ lessonStatus);
     
     //verify progress of the course
      String progressStatus=driver.findElement(By.xpath("//*[@class='ld-progress-heading']")).getText();
      System.out.println("Progress of course completion status is : "+ progressStatus);
    
	    }  
	    
	    @AfterClass
	    public void afterClass() {
	   driver.close();
	    }
	    }


