package Activity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_6 {
	 WebDriver driver;
	@BeforeClass
    public void beforeClass() {
   	driver = new FirefoxDriver();
		
	//Open the browser
	driver.get("https://alchemy.hguy.co/lms");
    }
	
    @Test
    public void TestCase() {
    	//Find the navigation bar.
        String navbar = driver.findElement(By.cssSelector("#site-navigation")).getText();
        
   		System.out.println("navigation bar is:" + navbar);
   		
   		//Select the menu item that says �My Account� and click it.
   		
   		driver.findElement(By.linkText("My Account")).click();
   		
   		//Read the page title and verify that you are on the correct page.

   		String pageTitle = driver.getTitle();
   	    System.out.println("Title of the page is : "+ pageTitle);
   	    
   	    //Find the �Login� button on the page and click it.
   	    
   	 driver.findElement(By.linkText("Login")).click();
   	
   	   //Find the username field of the login form and enter the username into that field.
   	 driver.findElement(By.id("user_login")).sendKeys("root");
   	 
   	 //Find the password field of the login form and enter the password into that field.
   	 driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd"); 
   	    
   	    //Find the login button and click it.
   	   driver.findElement(By.className("login-submit")).click();
   	   
   	    //Verify that you have logged in.
   	   String Loggedin= driver.findElement(By.cssSelector("span.display-name:nth-child(1)")).getText();
   	   System.out.println("Verify successfully logged in: " + Loggedin);
   	   
    }
    
    
    @AfterClass
    public void afterClass() {
    driver.close();
    }
    }
