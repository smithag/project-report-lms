package Activity1;

import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import org.testng.Assert;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Acitivity_1 {
	 WebDriver driver;
	@BeforeClass
    public void beforeClass() {
   	driver = new FirefoxDriver();
		
	//Open the browser
	driver.get("https://alchemy.hguy.co/lms");
    }
	
    @Test
    public void TestCase() {
	//Find the page title and print it
   
	String pageTitle = driver.getTitle();
    System.out.println("Title of the website is : "+ pageTitle);
    Assert.assertEquals(pageTitle, "Alchemy LMS � An LMS Application");
    }
    
    @AfterClass
    public void afterClass() {
    driver.close();
    }
    }
