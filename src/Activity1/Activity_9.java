package Activity1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class Activity_9 {
 
	 WebDriver driver;
		@BeforeClass
	    public void beforeClass() {
	   	driver = new FirefoxDriver();
			
		//Open the browser
		driver.get("https://alchemy.hguy.co/lms");
	    }
		
	    @Test
	    public void TestCase() {
	    //Find the navigation bar.
	        String navbar = driver.findElement(By.cssSelector("#site-navigation")).getText();
	        
	   		System.out.println("navigation bar is:" + navbar);
	   		driver.findElement(By.linkText("My Account")).click();
	   	    driver.findElement(By.linkText("Login")).click();
	   	    driver.findElement(By.id("user_login")).sendKeys("root");
	   	    driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd"); 
	        driver.findElement(By.className("login-submit")).click();
	   	//Select the menu item that says �All Courses� and click it.
	   		driver.findElement(By.linkText("All Courses")).click();
	   		
	    //Select any course and open it.
             driver.findElement(By.xpath("//a[contains(@href,'social-media') and @role='button']")).click();
             driver.findElement(By.className("ld-item-title")).click();
             String Title = driver.getTitle();
	   	     System.out.println("Title of the Topic is : "+ Title);
   		//Click on a lesson in the course. Verify the title of the course.
	   		 driver.findElement(By.linkText("This is the First Topic")).click();
	   
	   		 String pageTitle = driver.getTitle();
	   	     System.out.println("Title of the lesson is : "+ pageTitle);
	   		
	    //Click the Mark Complete button on the page (if available).
            
	   	    String status = driver.findElement(By.cssSelector(".ld-status")).getText();
		     System.out.println("status is : "+ status);
		         
		    }  
		    
		    @AfterClass
		    public void afterClass() {
		   driver.close();
		    }
		    }

