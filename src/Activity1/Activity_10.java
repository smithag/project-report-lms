package Activity1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;



public class Activity_10 {
 
	 WebDriver driver;
		@BeforeClass
	    public void beforeClass() {
	   	driver = new FirefoxDriver();
	   	
			
		//Open the browser
		driver.get("https://alchemy.hguy.co/lms");
	    }
		
	    @Test
	    public void TestCase() {
	    //Find the navigation bar.
	    	
	        String navbar = driver.findElement(By.cssSelector("#site-navigation")).getText();
	        
	   		System.out.println("navigation bar is:" + navbar);
	   		driver.findElement(By.linkText("My Account")).click();
	   	    driver.findElement(By.linkText("Login")).click();
	   	    driver.findElement(By.id("user_login")).sendKeys("root");
	   	    driver.findElement(By.id("user_pass")).sendKeys("pa$$w0rd"); 
	        driver.findElement(By.className("login-submit")).click();
	   	//Select the menu item that says �All Courses� and click it.
	   		driver.findElement(By.linkText("All Courses")).click();
	   		
	    //Select any course and open it.
             driver.findElement(By.xpath("//a[contains(@href,'social-media') and @role='button']")).click();
             driver.findElement(By.className("ld-item-title")).click();
             String Title = driver.getTitle();
	   	     System.out.println("Title of the Topic is : "+ Title);

   		//Click on a lesson in the course. Verify the title of the course.
	   		 driver.findElement(By.linkText("This is the First Topic")).click();
	   
	   		 String pageTitle1 = driver.getTitle();
	   	     System.out.println("Title of the lesson1 is : "+ pageTitle1);
	   		
	   	// Open a topic in that lesson. Mark it as complete.
	   	  driver.findElement(By.linkText("Next Topic")).click();
	   	 String pageTitle2 = driver.getTitle();
  	     System.out.println("Title of the lesson2 is : "+ pageTitle2);
  	       
  	    
  	       	driver.findElement(By.partialLinkText("Basic Investment")).click();
  	      
  		   	 String pageTitle3 = driver.getTitle();
	     System.out.println("Title of the lesson3 is : "+ pageTitle3);
	   	     
	   	//Mark all the topics in the lesson as complete (if available).
	      driver.findElement(By.linkText("Back to Lesson")).click();
	 	   String topicStatus = driver.findElement(By.cssSelector(".ld-status")).getText();
	     System.out.println("Topic1 completion status is : "+ topicStatus);
	    
		    }  
		    
		    @AfterClass
		    public void afterClass() {
		   driver.close();
		    }
		    }


