package Activity1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;

public class Activity_4 {
 
	 WebDriver driver;
		@BeforeClass
	    public void beforeClass() {
	   	driver = new FirefoxDriver();
			
		//Open the browser
		driver.get("https://alchemy.hguy.co/lms");
	    }
		
	    @Test
	    public void TestCase() {
	    	//the title of the second most popular course
			String secondtitle = driver.findElement(By.cssSelector("#post-71 > div:nth-child(3) > h3:nth-child(1)")).getText();
			
			Assert.assertTrue(true,"Email Marketing Strategies");
			System.out.println("Heading of the webpage is: " + secondtitle);
	    }
	    
	    @AfterClass
	    public void afterClass() {
	    driver.close();
	    }
	    }
	    