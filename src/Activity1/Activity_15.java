package Activity1;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class Activity_15 {
 
	 WebDriver driver;
		@BeforeClass
	    public void beforeClass() {
	   	driver = new FirefoxDriver();
	   				
		//Open the browser
		driver.get("https://alchemy.hguy.co/lms");
		}
		 @Test(priority=0)
		    public void TestCase1() {
		    //Find the navigation bar.
			  String navbar = driver.findElement(By.xpath("//*[@id='site-navigation']")).getText();
		        
		   		System.out.println("navigation bar is:" + navbar);
		   		driver.findElement(By.xpath("//*[@id='menu-item-1507']")).click();
		   	    driver.findElement(By.xpath("//*[contains(text(),'Login')]")).click();
		   	    driver.findElement(By.xpath("//*[@id='user_login']")).sendKeys("root"); 
		   	    driver.findElement(By.xpath("//*[@id='user_pass']")).sendKeys("pa$$w0rd"); 
		        driver.findElement(By.xpath("//*[@id='wp-submit']")).click();
	//Select the menu item that says �All Courses� and click it.
		   		driver.findElement(By.xpath("//*[@id='menu-item-1508']")).click();
		   		
//Select any course and open it.
		        driver.findElement(By.xpath("//*[@id='post-69']")).click();
		        String cTitle = driver.getTitle();
		   	     System.out.println("Title of the course is : "+ cTitle);
		        driver.findElement(By.xpath("//*[@class='ld-item-title']")).click();
		        String Title = driver.getTitle();
		   	     System.out.println("Title of the Topic is : "+ Title);
//Click on a lesson in the course. Verify the title of the course.
		   		 driver.findElement(By.xpath("//*[@class='ld-topic-title']")).click();
		   
		   		 String pageTitle = driver.getTitle();
		   	     System.out.println("Title of the lesson1 is : "+ pageTitle);
		   		
		   	        		
	 // Open a topic in that lesson. Mark it as complete.
		   	       driver.findElement(By.xpath("//*[@class='ld-content-action']")).click();
		   	       String pageTitle2 = driver.getTitle();
			       System.out.println("Title of the lesson2 is : "+ pageTitle2);
			       
			   
			       	driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/div[2]/a")).click();
			        String pageTitle3 = driver.getTitle();
		            System.out.println("Title of the lesson3 is : "+ pageTitle3);
	//Mark all the topics in the lesson as complete (if available).
				     driver.findElement(By.xpath("//*[@class='ld-primary-color']")).click();
				     String topicStatus = driver.findElement(By.xpath("//*[@class='ld-lesson-list-progress']")).getText();
				     System.out.println("Topic1 completion status is : "+ topicStatus);
		}
		  
		 @Test(priority=1)
         public void TestCase2() {
			  driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[4]/a")).click();
				driver.findElement(By.xpath("//*[@id='ld-expand-85']")).click();
				 String Title2= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
				  System.out.println("Title of the Topic2 is : "+ Title2);
						    
				driver.findElement(By.xpath("//*[@id='ld-table-list-item-200']")).click();
				 String Tlesson4=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
				System.out.println("Title of the lesson4 is : "+ Tlesson4);		
				  
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/div[2]/a")).click();
				String Tlesson5= driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
				System.out.println("Title of the lesson5 is : "+ Tlesson5);	
				
				driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/a")).click();
				String topicStatus2=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/div/div[1]/div[2]/span[1]")).getText();
				 System.out.println("Topic2 completion status is : "+ topicStatus2);
      	   
         }
		    
		  @Test(priority=2)
         public void TestCase3() {
			  driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[4]/a")).click();
				 driver.findElement(By.xpath("//*[@id='ld-expand-87']")).click();
				 String Title3=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/h1")).getText();
				 System.out.println("Title of the Topic3 is : "+ Title3);
				 
				 String topicStatus3=driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[2]/div/div/div/div/div[1]/div/div[1]")).getText();
				 System.out.println("Topic3 completion status is : "+ topicStatus3);
				 
				 driver.findElement(By.xpath("/html/body/div[1]/div/div[2]/div[2]/div/div/div[3]/a")).click();
				 String cprogress = driver.findElement(By.cssSelector(".ld-progress-percentage")).getText();
				
				 System.out.println("Course completion status is : "+ cprogress);
				 
				 driver.findElement(By.xpath("//*[@id='menu-item-1507']")).click();
				String logout= driver.findElement(By.xpath("/html/body/div[1]/div/div/div/main/article/div/section[2]/div[2]/div[2]/div[2]/div[4]/a")).getText();
				System.out.println("logged out from Myaccount : "+ logout);
		  }  
			   	
			     @AfterClass
				    public void afterClass() {
				 driver.close();
				    }
				    }
