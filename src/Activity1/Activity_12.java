package Activity1;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;



public class Activity_12 {
 

	 WebDriver driver;
		@BeforeClass
	    public void beforeClass() {
	   	driver = new FirefoxDriver();
			
		//Open the browser
		driver.get("https://alchemy.hguy.co/lms");
	    }
		
	    @Test
	    public void TestCase() {
	    //Find the navigation bar.
	        String navbar = driver.findElement(By.xpath("//*[@id='site-navigation']")).getText();
	        
	   		System.out.println("navigation bar is:" + navbar);
	   		driver.findElement(By.xpath("//*[@id='menu-item-1507']")).click();
	   	    driver.findElement(By.xpath("//*[contains(text(),'Login')]")).click();
	   	    driver.findElement(By.xpath("//*[@id='user_login']")).sendKeys("root"); 
	   	    driver.findElement(By.xpath("//*[@id='user_pass']")).sendKeys("pa$$w0rd"); 
	        driver.findElement(By.xpath("//*[@id='wp-submit']")).click();
	   	//Select the menu item that says �All Courses� and click it.
	   		driver.findElement(By.xpath("//*[@id='menu-item-1508']")).click();
	   		
	    //Select any course and open it.
            driver.findElement(By.xpath("//*[@id='post-69']")).click();
            String cTitle = driver.getTitle();
	   	     System.out.println("Title of the course is : "+ cTitle);
            driver.findElement(By.xpath("//*[@class='ld-item-title']")).click();
            String Title = driver.getTitle();
	   	     System.out.println("Title of the Topic is : "+ Title);
  		//Click on a lesson in the course. Verify the title of the course.
	   		 driver.findElement(By.xpath("//*[@class='ld-topic-title']")).click();
	   
	   		 String pageTitle = driver.getTitle();
	   	     System.out.println("Title of the lesson is : "+ pageTitle);
	   		
	    //Click the Mark Complete button on the page (if available).
           
	   	    String status = driver.findElement(By.xpath("//*[@class='ld-status ld-status-complete ld-secondary-background']")).getText();
		     System.out.println("status is : "+ status);
		         
		    }  
		    
		    @AfterClass
		    public void afterClass() {
		   driver.close();
		    }
		    }

