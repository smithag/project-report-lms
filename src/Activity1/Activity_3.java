package Activity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_3 {
	 WebDriver driver;
	@BeforeClass
    public void beforeClass() {
   	driver = new FirefoxDriver();
		
	//Open the browser
	driver.get("https://alchemy.hguy.co/lms");
    }
	
    @Test
    public void TestCase1() {
    	WebElement firstinfotitle = driver.findElement(By.cssSelector("h3.uagb-ifb-title"));
    	String infotext = firstinfotitle.getText();
    	System.out.println("The First Info box title is :" + infotext);
      //Using the contentEquals() Class
    	boolean contentEqualsVerify = infotext.contentEquals("Actionable Training");
    	System.out.println("Title equals Heading : " + contentEqualsVerify);
    }
    
    @AfterClass
    public void afterClass() {
    	driver.close();
    }
}
