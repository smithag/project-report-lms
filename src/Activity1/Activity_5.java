package Activity1;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Activity_5 {
	 WebDriver driver;
	@BeforeClass
    public void beforeClass() {
   	driver = new FirefoxDriver();
		
	//Open the browser
	driver.get("https://alchemy.hguy.co/lms");
    }
	
    @Test
    public void TestCase() {
    	//Find the navigation bar.
        String navbar = driver.findElement(By.cssSelector("#site-navigation")).getText();
        
   		System.out.println("navigation bar is:" + navbar);
   		
   		//Select the menu item that says �My Account� and click it.
   		
   		driver.findElement(By.linkText("My Account")).click();
   		
   		//Read the page title and verify that you are on the correct page.

   		String pageTitle = driver.getTitle();
   	    System.out.println("Title of the page is : "+ pageTitle);
    }
    
    @AfterClass
    public void afterClass() {
    driver.close();
    }
    }
    